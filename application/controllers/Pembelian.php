<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("pembelian_model");
		$this->load->model("supplier_model");
		$this->load->model("barang_model");
		
		//load validasi
		$this->load->library('form_validation');
		
			// cek login akses
		$user_login = $this->session->userdata();
		if (count($user_login) <= 1) {
			redirect("auth/index", "refresh");
	}
	
	}

	public function index()
	{
		$this->listPembelian();
    }
    
    public function listPembelian()
	{
		$data['data_pembelian'] = $this->pembelian_model->tampilDataPembelian();
        
		$data['content'] = 'forms/list_pembelian';
		$this->load->view('home2', $data);
    }
    
    public function input()
	{
      
        $data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
        
        
        //if (!empty($_REQUEST)) {
           // $pembelian_header = $this->pembelian_model;
           // $pembelian_header->savePembelianHeader();
            //$id_terakhir = array();
            //panggil ID transaksi terakhir
            //$id_terakhir = $pembelian_header->idTransaksiTerakhir();
           
			//redirect("pembelian/inputDetail/" . $id_terakhir, "refresh");
        //}
        $validation = $this->form_validation;
		$validation->set_rules($this->pembelian_model->rules());
		
		if ($validation->run()) {
			$this->pembelian_model->savePembelianHeader();
			$this->session->set_flashdata('info', '<div style="color : green">simpan data berhasil !</div>');
			redirect("pembelian/index", "refresh");
		}
			
		//$this->load->view('input_supplier');
		$data['content'] = 'forms/input_pembelian_header';
		$this->load->view('home2', $data);
        
		
	}
    public function inputDetail($id_pembelian_header)
	{
        // panggil data barang untuk kebutuhan form input
		 $data['id_header'] = $id_pembelian_header;
         $data['data_barang'] = $this->barang_model->tampilDataBarang();
         $data['data_pembelian_detail'] = $this->pembelian_model->tampilDataPembelianDetail($id_pembelian_header);
        
        //if (!empty($_REQUEST)) {
          
            //$this->pembelian_model->savePembelianDetail($id_pembelian_header);
            
          
            //$kode_barang  = $this->input->post('kode_barang');
            //$qty        = $this->input->post('qty');
            //$this->barang_model->updateStok($kode_barang, $qty);

			//redirect("pembelian/inputDetail/" . $id_pembelian_header, "refresh");
        //}
         $validation = $this->form_validation;
		$validation->set_rules($this->pembelian_model->rules1());
		
		if ($validation->run()) {
			$this->pembelian_model->savePembelianDetail($id_pembelian_header);
			$this->session->set_flashdata('info', '<div style="color : green">simpan data berhasil !</div>');
			redirect("pembelian/inputDetail/" . $id_pembelian_header, "refresh");
		}
			
		//$this->load->view('input_supplier');
		$data['content'] = 'forms/input_pembelian_detail';
		$this->load->view('home2', $data);
        
		
		//$this->load->view('input_pembelian_detail', $data);
	}
	
	public function deletepembelian($id_pembelian_h)
	{
		$m_pembelian = $this->pembelian_model;
		$m_pembelian->delete($id_pembelian_h);
		redirect("Pembelian/index", "refresh");
		
	}
	
	public function report(){
		$data['content'] = 'forms/report';
		$this->load->view('home2', $data);
		
	}
	
	public function report_pembelian(){
		$data['data_pembelian']  = $this->pembelian_model->tampilreportpembelian();
		$data['content'] = 'forms/form_pembelian';
		$this->load->view('home2', $data);
		
	}
	
}
